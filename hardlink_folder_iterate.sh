#!/bin/sh
#Copies the folder structure from A and hardlinks every file inside it to B.
#To use it as a git hook change the directory in/out variables.
DIRECTORY_IN="$1/*"
DIRECTORY_OUT=$2
find $DIRECTORY_IN -type d -exec sh -c 'mkdir -p $1/${0#*/}' {} $DIRECTORY_OUT \;
find $DIRECTORY_IN -type f -exec sh -c 'ln -f $0 $1/${0#*/}' {} $DIRECTORY_OUT \;